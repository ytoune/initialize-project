#!/usr/bin/env node
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_extra_1 = require("fs-extra");
var path_1 = require("path");
var command_line_args_1 = __importDefault(require("command-line-args"));
var child_process_1 = require("child_process");
var reject = Promise.reject;
var main = function () { return __awaiter(void 0, void 0, void 0, function () {
    var options, dir, directory, files, packageJsonPath, packageJsonCont;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                options = command_line_args_1.default([{ name: 'dir', defaultOption: true }]);
                dir = options.dir;
                if ('string' !== typeof dir || !dir)
                    throw new Error('dir is required');
                directory = path_1.resolve(process.cwd(), dir);
                return [4 /*yield*/, fs_extra_1.stat(directory).then(function (s) { return s.isDirectory() || reject(new Error(dir + " is not directory")); }, function (x) { var _a; return ('ENOENT' === ((_a = x) === null || _a === void 0 ? void 0 : _a.code) ? fs_extra_1.ensureDir(directory) : reject(x)); })];
            case 1:
                _a.sent();
                files = [
                    '.editorconfig',
                    '.eslintignore',
                    '.eslintrc.yml',
                    ['_gitignore', '.gitignore'],
                    ['_src', 'src'],
                    '.vscode',
                    'scripts',
                    'tsconfig.build.json',
                    'tsconfig.json',
                ];
                return [4 /*yield*/, Promise.all(files.map(function (file) { return __awaiter(void 0, void 0, void 0, function () {
                        var _a, source, target;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = 'string' === typeof file ? [file, file] : file, source = _a[0], target = _a[1];
                                    return [4 /*yield*/, fs_extra_1.copy(path_1.join(__dirname, '..', source), path_1.join(directory, target), {
                                            overwrite: false,
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/];
                            }
                        });
                    }); }))];
            case 2:
                _a.sent();
                packageJsonPath = path_1.join(directory, 'package.json');
                return [4 /*yield*/, fs_extra_1.readFile(packageJsonPath, 'utf-8')
                        .then(function (t) { return JSON.parse(t); })
                        .catch(function (x) { var _a; return 'ENOENT' !== ((_a = x) === null || _a === void 0 ? void 0 : _a.code) && reject(x); })];
            case 3:
                packageJsonCont = _a.sent();
                return [4 /*yield*/, fs_extra_1.writeFile(path_1.join(directory, 'package.json'), JSON.stringify(__assign({ name: path_1.basename(directory), version: '0.0.1', description: '', author: { name: 'ytoune' }, engines: { node: '>= 8' }, license: 'MIT', private: true, scripts: {
                            build: 'ts-node scripts/prebuild.ts && tsc --build tsconfig.build.json',
                            release: 'npm run build && ts-node scripts/release.ts',
                            test: "eslint 'src/**/*.ts' && tsc --noEmit && jest",
                        }, dependencies: {}, devDependencies: {}, jest: { transform: { '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest' } } }, packageJsonCont), null, 2), { encoding: 'utf8' })];
            case 4:
                _a.sent();
                return [4 /*yield*/, new Promise(function (res, rej) {
                        return child_process_1.spawn('yarn', [
                            'add',
                            '-D',
                            '@types/fs-extra',
                            '@types/gh-pages',
                            '@types/jest',
                            '@types/node',
                            'fs-extra',
                            'gh-pages',
                            '@typescript-eslint/eslint-plugin',
                            '@typescript-eslint/parser',
                            'eslint',
                            'eslint-config-prettier',
                            'eslint-plugin-prettier',
                            'prettier',
                            'ts-node',
                            'typescript',
                            'jest',
                            'ts-jest',
                        ], { cwd: directory })
                            .on('close', res)
                            .on('error', rej);
                    })];
            case 5:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
main()
    .then(function () {
    console.log('Done!!');
})
    .catch(function (x) {
    console.error(x);
    process.exit(1);
});
