#!/usr/bin/env node
import { stat, ensureDir, copy, writeFile, readFile } from 'fs-extra'
import { resolve, join, basename } from 'path'
import commandLineArgs from 'command-line-args'
import { spawn } from 'child_process'

const { reject } = Promise

const main = async () => {
	const options = commandLineArgs([{ name: 'dir', defaultOption: true }])
	const { dir } = options
	if ('string' !== typeof dir || !dir) throw new Error('dir is required')
	const directory = resolve(process.cwd(), dir)
	await stat(directory).then(
		s => s.isDirectory() || reject(new Error(`${dir} is not directory`)),
		x => ('ENOENT' === x?.code ? ensureDir(directory) : reject(x)),
	)
	const files: (string | [string, string])[] = [
		'.editorconfig',
		'.eslintignore',
		'.eslintrc.yml',
		['_gitignore', '.gitignore'],
		['_src', 'src'],
		'.vscode',
		'scripts',
		'tsconfig.build.json',
		'tsconfig.json',
	]
	await Promise.all(
		files.map(async file => {
			const [source, target] = 'string' === typeof file ? [file, file] : file
			await copy(join(__dirname, '..', source), join(directory, target), {
				overwrite: false,
			})
		}),
	)
	const packageJsonPath = join(directory, 'package.json')
	const packageJsonCont = await readFile(packageJsonPath, 'utf-8')
		.then(t => JSON.parse(t))
		.catch(x => 'ENOENT' !== x?.code && reject(x))
	await writeFile(
		join(directory, 'package.json'),
		JSON.stringify(
			{
				name: basename(directory),
				version: '0.0.1',
				description: '',
				author: { name: 'ytoune' },
				engines: { node: '>= 10' },
				license: 'MIT',
				private: true,
				scripts: {
					build:
						'ts-node scripts/prebuild.ts && tsc --build tsconfig.build.json',
					release: 'npm run build && ts-node scripts/release.ts',
					test: "eslint 'src/**/*.ts' && tsc --noEmit && jest",
				},
				dependencies: {},
				devDependencies: {},
				jest: { transform: { '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest' } },
				...packageJsonCont,
			},
			null,
			2,
		),
		{ encoding: 'utf8' },
	)
	await new Promise<number>((res, rej) =>
		spawn(
			'yarn',
			[
				'add',
				'-D',
				'@types/fs-extra',
				'@types/gh-pages',
				'@types/jest',
				'@types/node',
				'fs-extra',
				'gh-pages',
				'@typescript-eslint/eslint-plugin',
				'@typescript-eslint/parser',
				'eslint',
				'eslint-config-prettier',
				'eslint-plugin-prettier',
				'prettier',
				'ts-node',
				'typescript',
				'jest',
				'ts-jest',
			],
			{ cwd: directory },
		)
			.on('close', res)
			.on('error', rej),
	)
}

main()
	.then(() => {
		console.log('Done!!')
	})
	.catch(x => {
		console.error(x)
		process.exit(1)
	})
